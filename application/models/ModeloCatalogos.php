<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function Insert($tabla,$data){
        $this->db->insert($tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($tabla);
        return $id;
    }
    public function deleteCatalogo($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }
    function getselectvaluerowwhere($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function getselectvaluerowwhereorderby($table,$where,$order){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($order);
        $query=$this->db->get(); 
        return $query;
    }
    function datoscorreopaquete($id){
        $strq = "SELECT sis.sistema, paq.paquete,paq.costo_mensual,paq.costo_anual 
                FROM sistema_paquetes as paq
                inner JOIN sistemas as sis on sis.sisId=paq.sistemaId
                WHERE paq.paqId=".$id;
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
  

}
