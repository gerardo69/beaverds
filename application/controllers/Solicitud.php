<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitud extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
    }
	public function index()	{
		$data['navlink']=base_url().'Inicio';
		$idsp = $this->input->post('idsp');
		$paquete1_where = array('paqId' => $idsp);
		

		$paquete1_row=$this->ModeloCatalogos->getselectvaluerowwhere('sistema_paquetes',$paquete1_where);
		
		$date['paquete1_id']=0;
		$date['paquete1_name']=0;
		$date['paquete1_mes']=0;
		$date['paquete1_anual']=0;
		$date['paquete1_detalle']='';
		foreach ($paquete1_row->result() as $item) {
			$date['paquete1_id']=$item->paqId;
			$date['paquete1_name']=$item->paquete;
			$date['paquete1_mes']=$item->costo_mensual;
			$date['paquete1_anual']=$item->costo_anual;

			$detalle1_where = array('paqId' => $item->paqId);
			$paqueted1_row=$this->ModeloCatalogos->getselectvaluerowwhereorderby('sistema_paquete_detalle',$detalle1_where,'orden asc');
			foreach ($paqueted1_row->result() as $item) {
				$date['paquete1_detalle'].='<li>'.$item->detalle.'</li>';
			}
			
		}

		$data['title']='BeaverDS Desarrollo Web | Diseño Paginas Web en Puebla, Publicidad en Facebook y Google, Software a la Medida';
		$data['menuquery']=$this->ModeloCatalogos->getselectvaluerowwhere('menu',array('activo'=>1));
		$this->load->view('theme/header',$data);
		$this->load->view('solicitud',$date);
		$this->load->view('theme/footer');
	}
	function enviada(){
		$data = $this->input->post();
		//echo json_encode($data);
		$data['direccioncli']=$_SERVER['REMOTE_ADDR'];
		$id=$this->ModeloCatalogos->Insert('solicitudes',$data);
		$this->mail($id);
		$data['title']='BeaverDS Desarrollo Web | Diseño Paginas Web en Puebla, Publicidad en Facebook y Google, Software a la Medida';
		$data['menuquery']=$this->ModeloCatalogos->getselectvaluerowwhere('menu',array('activo'=>1));
		$this->load->view('theme/header',$data);
		$this->load->view('solicitud_e');
		$this->load->view('theme/footer');
		
	}
	function mail($id){
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.beaverds.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'contacto@beaverds.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'B=DWM(l!cK91';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('contacto@beaverds.com','Contacto');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
              $where = array('solId'=>$id);
            $datos=$this->ModeloCatalogos->getselectvaluerowwhere('solicitudes',$where);
            

            foreach ($datos->result() as $item) {
            		$serv = $item->serv;
                    $correo=$item->correo;
                    $asunto = $item->asunto;

                    $nombre = $item->nombre;
                    $telefono = $item->telefono;
                    $mensaje = $item->mensaje;
                    $Subdominio=$item->Subdominio;
                    $dominio=$item->dominio;
                    
            }
            $datospaquete=$this->ModeloCatalogos->datoscorreopaquete($serv);
            foreach ($datospaquete->result() as $item) {
            	$sistema = $item->sistema;
            	$paquete = $item->paquete;
            	$costo_mensual = $item->costo_mensual;
            	$costo_anual  = $item->costo_anual;
            }
            $this->email->bcc('contacto@beaverds.com');
        //======================
        

        
        //$this->email->to($email, $cliente);
        

        $contenido='<p>'.$nombre.'</p>
        			<p><a href="mailto:'.$correo.'" target="_top">'.$correo.'</a></p>
        			<p><a href="tel:'.$telefono.'" target="_top">'.$telefono.'</a></p>
        			<p>Sumdominio: '.$Subdominio.''.$dominio.'</p>
        			<p>'.$mensaje.'</p>
        			
        			<br>
        			<p>Sistema: '.$sistema.'</p>
        			<p>Paquete: '.$paquete.'</p>
        			<p>Costo mensual: $'.$costo_mensual.'</p>
        			<p>Costo anual: $'.$costo_anual.'</p>
        			';
        
      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)


        $message  = '<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<meta name="viewport" content="width=device-width">
						<title>BeaverDS</title>
					</head>
					<body bgcolor="#00B1E1" style="padding: 0; margin: 0; width: 100%; background:url(https://dm9d104elaxz5.cloudfront.net/Welcome%20series/backgroundPixel.png) top center / auto 100% repeat-x #0079bf; font-family: Helvetica, Arial, san-serif; max-width: 100%; background-color: #00B1E1;">
						<span style="color:transparent;visibility:hidden;display:none;opacity:0;height:0;width:0;font-size:0;">Solicitud de Sistema</span>
						<table bgcolor="#00B1E1" style="background-color:#00b1e1; background:url(https://dm9d104elaxz5.cloudfront.net/Welcome%20series/backgroundPixel.png) top center / auto 100% repeat-x #0079bf; font-family:Helvetica,Arial,san-serif; margin:0; max-width:100%; padding:0; width:100%">
							<tbody>
								<tr>
									<td>
										<center>
										<table style="margin:10px auto; max-width:100%; width:600" class="preheader">
											<tbody>
												
												<tr>
													<td style="text-align:center">
														<center>
														<a href="https://beaverds.com">
														<img height="40" width="130" alt="Beaverds Logo" style="display: block; margin:0 auto;" src="https://beaverds.com/public/images/logo_b.svg"></a>
														</center>
													</td>
												</tr>
											</tbody>
										</table>
										<table style="margin:0 auto; max-width:100%; width:600px" class="email-wrap">
											<tbody>
												<tr>
													<td><!-- CONTENT -->
													<style type="text/css">.content p{color:#444;font-size:18px;line-height:24px}</style>
													<table bgcolor="white" border="0" cellpadding="0" cellspacing="0" style="background:white; border-radius:8px; border:0; margin:10px auto; width:100%" class="content">
														<tbody>
															<tr>
																<td>&nbsp; <font color="#444444"> <!--/// BLOG POST LINKS --> </font>
																<table border="0" cellpadding="35" cellspacing="0">
																	<tbody><!-- -->  <!-- -->  <!-- -->
																	<tr>
																		<td align="left" style="color:#444444; font-size:18px; line-height:24px">
					                                                        
					                                                            '.$contenido.'
					                                                        
					                                                    </td>
																		</tr> <!-- -->
																	</tbody>
																</table>
																<font color="#444444"></font>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
								<style type="text/css">.msoFix{mso-table-lspace:-1pt;mso-table-rspace:-1pt}</style>
								<!-- FOOTER -->
								<table border="0" cellpadding="0" cellspacing="12" style="border:0; font-size:14px; line-height:20px; max-width:100%; width:100%">
									<tbody>
										<tr>
											<td>
												<center>
												<a style="text-decoration: none; padding:8px 0;" href="#">
													<img width="32" height="32" alt="" style="display: inline;" src="https://dm9d104elaxz5.cloudfront.net/Newsletter/facebook-social.png">
												</a>&nbsp;&nbsp;
												<a style="text-decoration: none; padding:8px 0;" href="#">
													<img width="32" height="32" alt="" style="display: inline;" src="https://dm9d104elaxz5.cloudfront.net/Newsletter/twitter-social.png">
												</a>&nbsp;&nbsp;
												<a style="text-decoration: none; padding:8px 0;" href="#">
													<img width="32" height="32" alt="" style="display: inline;" src="https://dm9d104elaxz5.cloudfront.net/Newsletter/linkedin-social.png">
												</a>&nbsp;&nbsp;
												<a style="text-decoration: none; padding:8px 0;" href="#">
													<img width="32" height="32" alt="" style="display: inline;" src="https://dm9d104elaxz5.cloudfront.net/Newsletter/insta-social.png">
												</a>
												</center>
											</td>
										</tr>
										<tr>
											<td>
												<center>
												<font color="white">
												<em>Copyright © 2019 BeaverDS., Todos los derechos reservados.<br>
												
												
												</em>
												</font>
												</center>
											</td>
										</tr>
									</tbody>
								</table>
								</center>
							</td>
						</tr>
					</tbody>
					</table>
					</body>
					</html>';

        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

        //==================
	}
}