<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
    }

	public function index()	{
		$data['navlink']='';
		$data['title']='BeaverDS Desarrollo Web | Diseño Paginas Web en Puebla, Publicidad en Facebook y Google, Software a la Medida';
		$data['menuquery']=$this->ModeloCatalogos->getselectvaluerowwhere('menu',array('activo'=>1));
		$this->load->view('theme/header',$data);
		$this->load->view('principal');
		$this->load->view('theme/footer');
		//$this->load->view('contactojs');

	}
}