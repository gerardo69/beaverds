<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puntodeventa extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
    }

	public function index()	{
		$data['canonical']=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$data['navlink']=base_url().'Inicio';
		$paquete1_where = array('paqId' => 1 );
		$paquete2_where = array('paqId' => 2 );
		$paquete3_where = array('paqId' => 3 );
		$paquete1_row=$this->ModeloCatalogos->getselectvaluerowwhere('sistema_paquetes',$paquete1_where);
		$paquete2_row=$this->ModeloCatalogos->getselectvaluerowwhere('sistema_paquetes',$paquete2_where);
		$paquete3_row=$this->ModeloCatalogos->getselectvaluerowwhere('sistema_paquetes',$paquete3_where);

		$data['paquete1_id']=0;
		$data['paquete1_name']=0;
		$data['paquete1_mes']=0;
		$data['paquete1_anual']=0;
		$data['paquete1_detalle']='';
		foreach ($paquete1_row->result() as $item) {
			$data['paquete1_id']=$item->paqId;
			$data['paquete1_name']=$item->paquete;
			$data['paquete1_mes']=$item->costo_mensual;
			$data['paquete1_anual']=$item->costo_anual;

			$detalle1_where = array('paqId' => $item->paqId);
			$paqueted1_row=$this->ModeloCatalogos->getselectvaluerowwhereorderby('sistema_paquete_detalle',$detalle1_where,'orden asc');
			foreach ($paqueted1_row->result() as $item) {
				$data['paquete1_detalle'].='<li>'.$item->detalle.'</li>';
			}
			
		}
		$data['paquete2_id']=0;
		$data['paquete2_name']=0;
		$data['paquete2_mes']=0;
		$data['paquete2_anual']=0;
		$data['paquete2_detalle']='';
		foreach ($paquete2_row->result() as $item) {
			$data['paquete2_id']=$item->paqId;
			$data['paquete2_name']=$item->paquete;
			$data['paquete2_mes']=$item->costo_mensual;
			$data['paquete2_anual']=$item->costo_anual;

			$detalle2_where = array('paqId' => $item->paqId);
			$paqueted2_row=$this->ModeloCatalogos->getselectvaluerowwhereorderby('sistema_paquete_detalle',$detalle2_where,'orden asc');
			foreach ($paqueted2_row->result() as $item) {
				$data['paquete2_detalle'].='<li>'.$item->detalle.'</li>';
			}
			
		}
		$data['paquete3_id']=0;
		$data['paquete3_name']=0;
		$data['paquete3_mes']=0;
		$data['paquete3_anual']=0;
		$data['paquete3_detalle']='';
		foreach ($paquete3_row->result() as $item) {
			$data['paquete3_id']=$item->paqId;
			$data['paquete3_name']=$item->paquete;
			$data['paquete3_mes']=$item->costo_mensual;
			$data['paquete3_anual']=$item->costo_anual;

			$detalle3_where = array('paqId' => $item->paqId);
			$paqueted3_row=$this->ModeloCatalogos->getselectvaluerowwhereorderby('sistema_paquete_detalle',$detalle3_where,'orden asc');
			foreach ($paqueted3_row->result() as $item) {
				$data['paquete3_detalle'].='<li>'.$item->detalle.'</li>';
			}
			
		}
		$data['title']='Punto de Venta |BeaverDS Diseño Paginas Web en Puebla, Software a la Medida';
		$data['menuquery']=$this->ModeloCatalogos->getselectvaluerowwhere('menu',array('activo'=>1));
		$this->load->view('theme/header',$data);
		$this->load->view('puntodeventa');
		$this->load->view('theme/footer');
		//$this->load->view('contactojs');

	}
}