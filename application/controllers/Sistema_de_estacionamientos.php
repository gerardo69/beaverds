<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_de_estacionamientos extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
    }
	public function index()	{
		$data['canonical']=(isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$data['navlink']=base_url().'Inicio';
		$paquete1_where = array('paqId' => 4 );
		$paquete1_row=$this->ModeloCatalogos->getselectvaluerowwhere('sistema_paquetes',$paquete1_where);


		$data['paquete1_id']=0;
		$data['paquete1_name']=0;
		$data['paquete1_mes']=0;
		$data['paquete1_anual']=0;
		$data['paquete1_detalle']='';
		foreach ($paquete1_row->result() as $item) {
			$data['paquete1_id']=$item->paqId;
			$data['paquete1_name']=$item->paquete;
			$data['paquete1_mes']=$item->costo_mensual;
			$data['paquete1_anual']=$item->costo_anual;

			$detalle1_where = array('paqId' => $item->paqId);
			$paqueted1_row=$this->ModeloCatalogos->getselectvaluerowwhereorderby('sistema_paquete_detalle',$detalle1_where,'orden asc');
			foreach ($paqueted1_row->result() as $item) {
				$data['paquete1_detalle'].='<li>'.$item->detalle.'</li>';
			}
			
		}

		$data['title']='BeaverDS | Sistema de estacionamientos,Diseño Paginas Web en Puebla,Desarrollo Web, Software a la Medida';
		$data['menuquery']=$this->ModeloCatalogos->getselectvaluerowwhere('menu',array('activo'=>1));
		$this->load->view('theme/header',$data);
		$this->load->view('estacionamiento');
		$this->load->view('theme/footer');
		//$this->load->view('estacionamientojs');

	}
}