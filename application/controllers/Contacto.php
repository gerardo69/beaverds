<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
    }
	public function index()	{
		$data['navlink']=base_url().'Inicio';
		$data['title']='Contacto | BeaverDS Desarrollo Web';
		$data['menuquery']=$this->ModeloCatalogos->getselectvaluerowwhere('menu',array('activo'=>1));
		$this->load->view('theme/header',$data);
		$this->load->view('contacto');
		$this->load->view('theme/footer');
		$this->load->view('contactojs');
	}
	function enviada(){
		$data = $this->input->post();
		unset($data['g-recaptcha-response']);
		$data['direccioncli']=$_SERVER['REMOTE_ADDR'];
		$id=$this->ModeloCatalogos->Insert('contacto',$data);
		//$this->mail($id);
	}
	
	
}