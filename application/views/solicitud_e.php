<section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-0 backblueinter" style="background-image: url(img/page-header/page-header-elements.jpg);" >
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center p-static order-2 text-center" style="margin-top: 45px;">
				<h1>Solicitud</h1>
			</div>
		</div>
	</div>
</section>
<section class="section section-height-2 border-0 mt-0 mb-0 pt-3">
	<div class="container py-2">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-lg-12">
				<div class="featured-boxes   m-0 mb-4 pb-3">
					<div class="featured-box featured-box-no-borders featured-box-box-shadow">
						<!--------------------------------->
							<div class="row mt-3 pb-4">
								<div class="col text-center">
									<h2 class="font-weight-semibold text-6 mb-0">Solicitud Enviada</h2>
									<p class="lead text-4 pt-2 font-weight-normal">En breve uno de nuestros asesores se comunicara con usted.</p>
								</div>
							</div>
						<!--------------------------------->

					</div>
				</div>
			</div>
		</div>
	</div>
</section>