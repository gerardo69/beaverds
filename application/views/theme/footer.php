</div><!--main--> 
			
			<footer id="footer" class="mt-0">
				<div class="container my-1">
					<div class="row py-1">
						<div class="col-lg-2 align-self-center mb-5 mb-lg-0">
							<a href="http://beaverds.com/">
								<img src="<?php echo base_url();?>public/images/logo.svg" class="img-fluid" alt="Beaverds" width="100">
							</a>
						</div>
						<div class="col-lg-3 mb-4 mb-lg-0">
							<h4 class="font-weight-normal text-color-light text-5 ls-0 mb-4">¿Quieres saber más?</h4>
							<p>Comunícate con un asesor a través de los medios disponibles:</p>
						</div>
						<div class="col-lg-3 mb-4 mb-lg-0">
							<h4 class="font-weight-normal text-color-light text-5 ls-0 mb-4">Detalles de contacto</h4>
							<ul class="list list-unstyled">
								<!--<li class="d-flex align-items-baseline"><i class="far fa-dot-circle text-color-primary mr-3"></i><div><span class="text-color-light">Direccion:</span> Puebla, puebla</div></li>-->
								<li class="d-flex align-items-baseline"><i class="fab fa-whatsapp text-color-primary mr-3"></i><div><span class="text-color-light">Telefono:</span> <a href="tel:222 328 5475">222 328 5475</a></div></li>
								<li class="d-flex align-items-baseline"><i class="far fa-envelope text-color-primary mr-3"></i><div><span class="text-color-light">Email:</span> <a href="mailto:contacto@beaverds.com">contacto@beaverds.com</a></div></li>
							</ul>
						</div>
						
						<div class="col-lg-4">
							
						</div>
					</div>
				</div>
				<div class="footer-copyright footer-copyright-style-2">
					<div class="container py-2">
						<div class="row justify-content-between py-2">
							<div class="col-auto">
								<p>© Copyright 2019. All Rights Reserved.</p>
							</div>
							

						</div>
					</div>
				</div>
			</footer>
		</div><!--body--> 

		<!-- Vendor -->
		<script src="<?php echo base_url();?>public/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/popper/umd/popper.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/common/common.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/vide/jquery.vide.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/vivus/vivus.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>public/js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>public/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo base_url();?>public/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Demo -->
		<script src="<?php echo base_url();?>public/js/demos/demo-sass.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>public/js/views/view.contact.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>public/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>public/js/theme.init.js"></script>
		<script src="<?php echo base_url();?>public/public/js/settings.js"></script>
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140938247-1"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->
		<script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-140938247-1');
    </script>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v3.3'
        });
      };

      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/es_ES/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="876584099370951"
      theme_color="#0084ff"
      logged_in_greeting="¡Hola! Como podemos ayudarte?"
      logged_out_greeting="¡Hola! Como podemos ayudarte?">
    </div>


	</body>
</html>
