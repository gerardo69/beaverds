<!DOCTYPE html>
<html lang="es">
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title><?php echo $title; ?></title>	

		<meta name="authoring-tool" content="BeaverDS,agb,rt">
        <meta name="description" content="Desarrollos de sistemas">
        <meta name="keywords" content="HTML,CSS,XML,JavaScript,punto de venta en puebla,ERP,software,Diseño de paginas web en Puebla,páginas web,precios diseño web,ingeniería web,BeaverDS,Creacion de paginas web,Tiendas virtuales,Sitios web en Puebla">
        <meta name="author" content="Beaverds">
        <meta name="og:description" content="Desarrollos de sistemas."/>
        <meta name="og:url" content="<?php echo base_url(); ?>"/>
        <meta name="og:title" content="Beaver DS"/>
        <meta name="og:image" content="<?php echo base_url(); ?>public/images/favicon.png"/>
        <meta name="geo.region" content="MX-PUE" />
        <meta name="theme-color" content="#2784b2">
		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url();?>public/images/logo_64.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo base_url();?>public/images/logo_64.ico">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,600,700" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>public/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public/vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>public/css/demos/demo-sass.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>public/css/skins/skin-sass.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>public/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>public/vendor/modernizr/modernizr.min.js"></script>
		<script src="<?php echo base_url();?>public/./vendor/three.r92.min.js"></script>
		<script src="<?php echo base_url();?>public/./dist/vanta.waves.min.js"></script>
		

	</head>
	<body data-spy="scroll" data-target=".header-nav-main nav" data-offset="80">

		<div class="body">
			<header id="header" class="header-transparent header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': false, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
				<div class="header-body border-top-0 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="200">
					<div class="header-container container-fluid px-lg-4">
						<div class="header-row px-lg-3">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo header-logo-sticky-change" style="width: 82px; height: 40px;">
										<a href="<?php echo base_url();?>">
											<img class="header-logo-non-sticky opacity-0" alt="Porto"  height="55" src="<?php echo base_url();?>public/images/logo_b.svg">
											<img class="header-logo-sticky opacity-0" alt="Porto"  height="55" src="<?php echo base_url();?>public/images/logo.svg">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-center">
								<div class="header-row">
									<div class="header-nav header-nav-line header-nav-bottom-line header-nav-light-text justify-content-center">
										<div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<?php foreach ($menuquery->result() as $item1) { 
															if($item1->principal==1){
																echo '<li class="dropdown">
																		<a class="dropdown-item dropdown-toggle current-page-active" href="'.base_url().$item1->menuurl.'">'.$item1->menu.'</a>';
															}else{
																echo '<li class="dropdown">
																		<a class="dropdown-item dropdown-toggle current-page-active">'.$item1->menu.' <i class="icon-arrow-down icons"></i></a>
																	<ul class="dropdown-menu">';
										$submenuquery=$this->ModeloCatalogos->getselectvaluerowwhere('menusub',array('activo'=>1,'menu'=>$item1->menuId));
																	foreach ($submenuquery->result() as $item2) {
																		echo '<li><a class="dropdown-item" href="'.base_url().$item2->submenuurl.'">'.$item2->submenu.'</a></li>';
																	}
																


																echo '</ul></li>';

															}
													?>
														

													<?php } ?>	

													
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
							<div class="header-column">
								<div class="header-row justify-content-end">
									<ul class="header-social-icons social-icons d-none d-sm-block social-icons-clean social-icons-icon-light">
										<li class="social-icons-facebook"><a href="https://www.facebook.com/beaverds" target="_blank" title="Facebook" rel="noopener"><i class="fab fa-facebook-f"></i></a></li>

										<li class="social-icons-instagram"><a href="https://www.instagram.com/beaverds_software" target="_blank" title="Instagram" rel="noopener"><i class="fab fa-instagram"></i></a></li>
									</ul>
									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main">
				<input type="hidden" id="base_url" value="<?php echo base_url();?>">