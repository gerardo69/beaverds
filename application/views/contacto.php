<style type="text/css">
    #mensaje-error,.vd_red{
        color:red !important;
        text-shadow: 2px 2px 2px #fff5f5;
        font-weight: bold;
    }
</style>
<section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-0 backblueinter" style="background-image: url(img/page-header/page-header-elements.jpg);" >
    <div class="container">
        <div class="row">
            <div class="col-md-12 align-self-center p-static order-2 text-center" style="margin-top: 45px;">
                <h1>Contacto</h1>
            </div>
        </div>
    </div>
</section>
<section class="section section-height-2 border-0 mt-0 mb-0 pt-3">
    <div class="container py-2">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-lg-12">
                <div class="featured-boxes   m-0 mb-4 pb-3">
                    <div class="featured-box featured-box-no-borders featured-box-box-shadow">
                        <!--------------------------------->
                            <div class="row mt-3 pb-4">
                                <div class="col-md-12">
                                    <form method="post" action="#" id="fomcontacto">
                                        <div class="row">
                                            <label class="col-md-3 required font-weight-bold text-dark text-2">Nombre</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="nombre" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 required font-weight-bold text-dark text-2">correo electrónico</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" name="correo" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 required font-weight-bold text-dark text-2">Teléfono</label>
                                            <div class="col-md-9">
                                                <input type="tel" class="form-control" name="telefono" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 required font-weight-bold text-dark text-2">Asunto</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="asunto" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 required font-weight-bold text-dark text-2">Tu Mensaje</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="mensaje" required></textarea>
                                            </div>
                                        </div>
                                       <div class="row">
                                           <div class="col-md-3"></div>
                                           <div class="col-md-9">
                                               <div class="g-recaptcha" data-sitekey="6Ld5YakUAAAAAAlTGkYdqZm_ybiKr40Kbk5v2rvL"></div>
                                           </div>
                                       </div> 
                                    </form>
                                </div>
                                <div class="col-md-12">
                                    <button  class="btn btn-primary btn-modern py-2 px-4 contactosubmit" >Solicitar</button>
                                </div>
                            
                            </div>
                        <!--------------------------------->

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>