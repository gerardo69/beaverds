<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="authoring-tool" content="beaver">
    <meta name="description" content="Desarrollos de sistemas">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript,punto de venta">
    <meta name="author" content="Beaver ds">
    <meta name="og:description" content="Desarrollos de sistemas."/>
    <meta name="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="og:title" content="Beaver DS"/>
    <meta name="og:image" content="<?php echo base_url(); ?>public/images/favicon.png"/>

    <link rel="icon" href="<?php echo base_url(); ?>public/images/favicon.ico" type="image/x-icon" />
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>public/images/favicon.png" type="image/png" />
    <link rel="apple-touch-icon" sizes="194x194" href="<?php echo base_url(); ?>public/images/favicon.png" type="image/png" />
    <meta name="theme-color" content="#2784b2">

    <title>BeaverDS</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/favicon.ico" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;Raleway:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.min.css">
    <!-- owl-carousel -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/owl-carousel/owl.carousel.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/font-awesome.css" />
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/magnific-popup/magnific-popup.css" />
    <!-- media element player -->
    <link href="<?php echo base_url(); ?>public/css/mediaelementplayer.min.css" rel="stylesheet" type="text/css" />
    <!-- Animate -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/animate.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/ionicons.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <!-- Responsive -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/responsive.css">
    <!-- custom style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/custom.css" />
    
</head>

<body>
    <!-- loading --
    <div id="loading">
        <div id="loading-center">
            <div class="loader">
                <div class="cube">
                    <div class="sides">
                        <div class="top"></div>
                        <div class="right"></div>
                        <div class="bottom"></div>
                        <div class="left"></div>
                        <div class="front"></div>
                        <div class="back"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- loading End -->
    <!-- Main Content -->
    <style type="text/css">
        .imgcomp{
            background: url(<?php echo base_url(); ?>public/images/compu.png);height: 27vh; 
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
        }
    </style>
    <div class="main-content iq-over-blue-90" data-jarallax-video="m4v:<?php echo base_url(); ?>public/video/01.m4v,webm:<?php echo base_url(); ?>public/video/01.webm,ogv:<?php echo base_url(); ?>public/video/01.ogv">
        <!--======= Coming Soon =======-->
        <section class="iq-coming overview-block-ptb text-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <img class="img-responsive center-block iq-mb-10 wow zoomIn" id="logo_img" data-wow-duration="1s" src="<?php echo base_url(); ?>public/images/logo_b.svg" alt="" style="visibility: visible; animation-duration: 1s; animation-name: zoomIn;">
                        <div class="big-text iq-tw-7 iq-mt-60" style="font-size: 4.5em">BeaverDs</div>
                        <h5 class="iq-font-light iq-tw-6 iq-mt-40">¡Estamos trabajando muy duro para darle la mejor experiencia posible!</h5>
                        <div class="row imgcomp" >
                            
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!--======= Coming Soon End =======-->
    </div>
    <!-- Main Content END -->
    <!-- back-to-top -->
    <div id="back-to-top">
        <a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i> </a>
    </div>
    <!-- back-to-top End -->
        
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url(); ?>public/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
    <!-- Main js -->
    <script src="<?php echo base_url(); ?>public/js/main.js"></script>
    

    <!-- Custom --
    <script src="js/custom.js"></script>-->
    <!-- Load Facebook SDK for JavaScript -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140938247-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-140938247-1');
    </script>
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v3.3'
        });
      };

      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/es_ES/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="876584099370951"
      theme_color="#0084ff"
      logged_in_greeting="¡Hola! Como podemos ayudarte?"
      logged_out_greeting="¡Hola! Como podemos ayudarte?">
    </div>
    
</body>

</html>