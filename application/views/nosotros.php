<section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-0 backblueinter" style="background-image: url(img/page-header/page-header-elements.jpg);" >
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center p-static order-2 text-center" style="margin-top: 45px;">
				<h1>Nosotros</h1>
			</div>
		</div>
	</div>
</section>
<section class="section section-height-2 border-0 mt-0 mb-0 pt-3">
	<div class="container py-2">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-lg-12">
				<div class="featured-boxes   m-0 mb-4 pb-3">
					<div class="featured-box featured-box-no-borders featured-box-box-shadow">
						<!--------------------------------->
							<div class="row mt-3 pb-4">
								<div class="col text-center" style="padding-top: 15px; padding-left: 35px; padding-right: 35px;">
									<div class="row">
										<p>Somos un grupo de profesionistas freenlacer enfocados en el desarrollo de software, siempre buscando innovar y estar a la vanguardia, usando las mejores tecnologías enfocandonos a las nececidades de nuestros clientes.</p>
									</div>
									<div class="row">
						                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
						                    <h5 class="title iq-tw-7">MISIÓN</h5>
						                    <p>Desarrollar software a la medida, especializado que cubra las necesidades del cliente, con los estandares mas altos en calidad y servicio.</p>
						                    
						                </div>
						                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
						                    <h5 class="title iq-tw-7">VISIÓN</h5>
						                    <p>Ser la empresa líder en desarrollo de sistemas de información, teniendo claro el objetivo de falicitar el dia a dia de las personas y empresas mediante la creación de tecnología web.</p>
						                    
						                </div>
						                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
						                    <h5 class="title iq-tw-7">VALORES</h5>
						                    <ul>
						                        <li>Honestidad</li>
						                        <li>Compromiso</li>
						                        <li>Respeto</li>
						                    </ul>
						                </div>
						            </div>
								</div>
							</div>
						<!--------------------------------->

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
			