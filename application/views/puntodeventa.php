<section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-0 backblueinter" style="background-image: url(img/page-header/page-header-elements.jpg);" >
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center p-static order-2 text-center" style="margin-top: 45px;">
				<h1>SOFTWARE PUNTO DE VENTA EN LA NUBE</h1>
			</div>
		</div>
	</div>
</section>
<section class="section section-height-2 border-0 mt-0 mb-0 pt-3">
	<div class="container py-2">
		<div class="row">
			<div class="col-md-3">
				<a  class="btn btn-dark btn-modern btn-outline py-2 px-4" href="#sprecios">Precios</a>
				<a  class="btn btn-primary btn-modern py-2 px-4" href="https://demo.beaverds.com">Demo</a>
			</div>
			<div class="col-md-6">
				<div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'autoplay': true, 'autoplayTimeout': 4000, 'margin': 10, 'animateIn': 'slideInDown', 'animateOut': 'slideOutDown'}">
					<div>
						<img alt="" class="img-fluid rounded" src="<?php echo base_url();?>public/images/pos/Imagen1.webp">
					</div>
					<div>
						<img alt="" class="img-fluid rounded" src="<?php echo base_url();?>public/images/pos/Imagen2.webp">
					</div>
					<div>
						<img alt="" class="img-fluid rounded" src="<?php echo base_url();?>public/images/pos/Imagen3.webp">
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<div class="container py-2">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-lg-12">
				<div class="featured-boxes   m-0 mb-4 pb-3">
					<div class="featured-box featured-box-no-borders featured-box-box-shadow" style="padding: 20px; text-align: left;">
						<!--------------------------------->
							<div class="row mt-3 pb-4">
								
									<div class="col-md-7">
										<h2 class="font-weight-semibold text-6 mb-0">El sistema PUNTO DE VENTA que buscas</h2>
										<p class="lead text-4 pt-2 font-weight-normal">El sistema punto de venta es intuitivo, potente y fácil de usar en las ventas diarias. Puede utilizarse con o sin lector de código y puede instalarse una impresora de tickets o no.</p>
										<div class="row">
											<div class="col-md-6">
												<ul class="list list-icons list-icons-style-3">
													<li><i class="fas fa-check"></i> Incrementa tus ventas.</li>
													<li><i class="fas fa-check"></i> Automatiza tu negocio.</li>
													<li><i class="fas fa-check"></i> Ahorra tiempo y dinero.</li>
												</ul>
											</div>
											<div class="col-md-6">
												<ul class="list list-icons list-icons-style-3">
													<li><i class="fas fa-check"></i> Reduce costos y errores.</li>
													<li><i class="fas fa-check"></i> Control de tu personal.</li>
													<li><i class="fas fa-check"></i> Evita robos de dinero o inventario.</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-5">
										<img width="100%" alt="dispositivos" src="<?php echo base_url();?>public/images/dispositivos.png">
									</div>
							</div>
							<div class="row mt-3 pb-4">
								<div class="col-md-12 text-center">
									<h2 style="margin-bottom: 50px;">¿Qué incluye?</h2>
								</div>
							</div>
							<div class="row mt-3 pb-4">
								<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
									<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>public/images/back.webp);">
										<div class="card-body text-center p-2">
											<i class="fas fa-sync-alt icons text-color-primary text-10"></i>
											<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold" style="min-height: 54px;">Actualizaciones periódicas</h4>
											<p class="card-text" style="min-height:130px;">Actualizaciones e innovaciones y constante mantenimiento y no tendrás que instalar nada.</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
									<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>public/images/back.webp);">
										<div class="card-body text-center p-2">
											<i class="fas fa-cloud icons text-color-primary text-10"></i>
											<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold" style="min-height: 54px;">Tu información estará más segura</h4>
											<p class="card-text" style="min-height:130px;">No deberás de preocuparte por tu información ya que estará segura y de igual forma podrás hacer tus propios respaldos de tu información para que siempre estés tranquilo.</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
									<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>public/images/back.webp);">
										<div class="card-body text-center p-2">
											<i class="fas fa-exchange-alt icons text-color-primary text-10"></i>
											<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold" style="min-height: 54px;">Información en tiempo real</h4>
											<p class="card-text" style="min-height:130px;">Por ser una aplicación en la nube, siempre podrás acceder a tu sistema sin importar en donde te encuentres o el horario.</p>
										</div>
									</div>
								</div>
								
							</div>
							<div class="row mt-3 pb-4">
								<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
									<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>public/images/back.webp);">
										<div class="card-body text-center p-2">
											<i class="fas fa-comments icons text-color-primary text-10"></i>
											<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold" style="min-height: 54px;">Soporte Técnico</h4>
											<p class="card-text" style="min-height:130px;">Si necesitas ayuda contactanos y en menos de 24 horas te daremos una solucion.</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
									<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>public/images/back.webp);">
										<div class="card-body text-center p-2">
											<i class="far fa-clock icons text-color-primary text-10"></i>
											<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold" style="min-height: 54px;">Tu información estará más segura</h4>
											<p class="card-text" style="min-height:130px;">Porque sabemos que tu tiempo son lo más importante, nosotros nunca te haremos instalar ningún tipo de software.</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
									<div class="card card-background-image-hover border-0" style="background-image: url(<?php echo base_url();?>public/images/back.webp);">
										<div class="card-body text-center p-2">
											<i class="fas fa-laptop icons text-color-primary text-10"></i>
											<h4 class="card-title mt-2 mb-2 text-5 font-weight-bold" style="min-height: 54px;">Compatibilidad con todos los dispositivo</h4>
											<p class="card-text" style="min-height:130px;">El Sistema se puede utilizar en cualquier dispositivo(tablet, computadora y/o movil) sin importar que sistema operativo tengas ya que funciona a través del navegador (Mozilla FireFox o Google Chrome).</p>
										</div>
									</div>
								</div>
								
							</div>

						<!--------------------------------->

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container py-2" id="sprecios">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-lg-12">
				<div class="featured-boxes   m-0 mb-4 pb-3">
					<div class="featured-box featured-box-no-borders featured-box-box-shadow" style="padding: 20px; text-align: left;">
						<!--------------------------------->
							<div class="pricing-table pricing-table-no-gap mb-4">
						<div class="col-md-4">
							<div class="plan">
								<div class="plan-header">
									<h3><?php echo $paquete1_name;?></h3>
								</div>
								<div class="plan-price">
									<span class="price"><span class="price-unit">$</span><?php echo $paquete1_mes;?></span>
									<label class="price-label">POR MES</label>
									<label class="price-label">ó</label>
									<label class="price-label">$<?php echo $paquete1_anual;?> anual</label>
								</div>
								<div class="plan-features">
									<ul>
										<?php echo $paquete1_detalle;?>
									</ul>
								</div>
								<div class="plan-footer">
									<form method="post" action="<?php echo base_url(); ?>Solicitud">
		                                <input type="number"  name="idsp" id="idsp" value="<?php echo $paquete1_id;?>" readonly style="display: none;">
		                                <button type="submit" class="btn btn-dark btn-modern btn-outline py-2 px-4">Solicitar</button>
		                            </form>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="plan plan-featured">
								<div class="plan-header bg-primary">
									<h3><?php echo $paquete2_name;?></h3>
								</div>
								<div class="plan-price">
									<span class="price"><span class="price-unit">$</span><?php echo $paquete2_mes;?></span>
									<label class="price-label">POR MES</label>
									<label class="price-label">ó</label>
									<label class="price-label">$<?php echo $paquete2_anual;?> anual</label>
								</div>
								<div class="plan-features">
									<ul>
										<?php echo $paquete2_detalle;?>
									</ul>
								</div>
								<div class="plan-footer">
									<form method="post" action="<?php echo base_url(); ?>Solicitud">
		                                <input type="number"  name="idsp" id="idsp" value="<?php echo $paquete2_id;?>" readonly style="display: none;">
		                                <button type="submit" class="btn btn-dark btn-modern btn-outline py-2 px-4">Solicitar</button>
		                            </form>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="plan">
								<div class="plan-header">
									<h3><?php echo $paquete3_name;?></h3>
								</div>
								<div class="plan-price">
									<span class="price"><span class="price-unit">$</span><?php echo $paquete3_mes;?></span>
									<label class="price-label">POR MES</label>
									<label class="price-label">ó</label>
									<label class="price-label">$<?php echo $paquete3_anual;?> anual</label>
								</div>
								<div class="plan-features">
									<ul>
										<?php echo $paquete3_detalle;?>
									</ul>
								</div>
								<div class="plan-footer">
									<form method="post" action="<?php echo base_url(); ?>Solicitud">
		                                <input type="number"  name="idsp" id="idsp" value="<?php echo $paquete3_id;?>" readonly style="display: none;">
		                                <button type="submit" class="btn btn-dark btn-modern btn-outline py-2 px-4">Solicitar</button>
		                            </form>
								</div>
							</div>
						</div>
					</div>
						<!--------------------------------->

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
			