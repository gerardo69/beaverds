<section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-0 backblueinter" style="background-image: url(img/page-header/page-header-elements.jpg);" >
    <div class="container">
        <div class="row">
            <div class="col-md-12 align-self-center p-static order-2 text-center" style="margin-top: 45px;">
                <h1>Solicitud</h1>
            </div>
        </div>
    </div>
</section>
<section class="section section-height-2 border-0 mt-0 mb-0 pt-3">
    <div class="container py-2">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-lg-10">
                <div class="featured-boxes   m-0 mb-4 pb-3">
                    <div class="featured-box featured-box-no-borders featured-box-box-shadow">
                        <!--------------------------------->
                            <div class="row mt-3 pb-4">
                                <div class="col ">
                                    <h2 class="font-weight-semibold text-center text-6 mb-0">Paquete: <?php echo $paquete1_name;?></h2>
                                    <h2 class="font-weight-semibold text-center text-5 mb-0">$ <?php echo $paquete1_mes;?> mensual</h2>
                                    <h2 class="font-weight-semibold text-center text-5 mb-0">$ <?php echo $paquete1_anual;?> anual</h2>
                                    <div class="col-lg-12 order-1 order-lg-2">
                                        <form method="post" class="contact-form" action="<?php echo base_url(); ?>Solicitud/enviada" method="POST" novalidate="novalidate">
                                            <input type="hidden" name="serv" value="<?php echo $paquete1_id;?>" required>

                                            <div class="row form-group">
                                                <label class="col-md-3 required font-weight-bold text-dark text-2 text-left">Subdominio</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="Subdominio" required title="Campo requerido">
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="form-control" name="dominio">
                                                        <option>.beaverds.com</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-3 required font-weight-bold text-dark text-2 text-left">Nombre</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="nombre" required title="Campo requerido">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-3 required font-weight-bold text-dark text-2 text-left">correo electrónico</label>
                                                <div class="col-md-9">
                                                    <input type="email" class="form-control" name="correo" required title="Campo requerido">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-3 required font-weight-bold text-dark text-2 text-left">Teléfono</label>
                                                <div class="col-md-9">
                                                    <input type="tel" class="form-control" name="telefono" required title="Campo requerido">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-3 required font-weight-bold text-dark text-2 text-left">Asunto</label>
                                                <div class="col-md-9 form-group">
                                                    <input type="text" class="form-control" name="asunto" required title="Campo requerido">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-3 required font-weight-bold text-dark text-2 text-left">Tu Mensaje</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" name="mensaje" required title="Campo requerido"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary btn-modern">Solicitar</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        <!--------------------------------->

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
