
				
				<section class="section bg-primary curved-border position-relative border-0 m-0" style="height: 100vh;" id="intro">
					
					<div class="appear-animation position-absolute" data-appear-animation="zoomIn" data-appear-animation-delay="1400" style="top: 25%; left: 16%;">
						<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-1.png" class="img-fluid" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 3, 'transition': true}" />
					</div>
					<div class="appear-animation position-absolute" data-appear-animation="zoomIn" data-appear-animation-delay="2000" style="top: 42%; left: 10%;">
						<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-2.png" class="img-fluid" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 3.5, 'transition': true, 'horizontal': true}" />
					</div>
					<div class="appear-animation position-absolute" data-appear-animation="zoomIn" data-appear-animation-delay="800" style="top: 60%; left: 20%;">
						<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-3.png" class="img-fluid" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 2.5, 'transition': true}" />
					</div>
					<div class="appear-animation position-absolute" data-appear-animation="zoomIn" data-appear-animation-delay="1100" style="top: 22%; right: 25%;">
						<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-4.png" class="img-fluid" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 2, 'transition': true, 'horizontal': true}" />
					</div>
					<div class="appear-animation position-absolute" data-appear-animation="zoomIn" data-appear-animation-delay="1700" style="top: 49%; right: 16%;">
						<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-5.png" class="img-fluid" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 3, 'transition': true}" />
					</div>
					<div class="appear-animation position-absolute" data-appear-animation="zoomIn" data-appear-animation-delay="2300" style="top: 71%; right: 13%;">
						<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-6.png" class="img-fluid" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 2.5, 'transition': true, 'horizontal': true}" />
					</div>

					<div class="container">
						<div class="row justify-content-center pt-4 mt-4">
							<div class="col-lg-7 text-center pt-3">
								<h1 class="text-color-light font-weight-extra-bold text-12 line-height-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">BEAVERDS</h1>
								<h1 class="text-color-light font-weight-extra-bold text-9 line-height-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">DESARROLLO WEB A LA MEDIDA</h1>
								<div class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="700">
									<p class="text-color-light opacity-6 text-5 pb-3 mb-4">Diseñamos y programamos el proyecto que tiene en mente..</p>
								</div>
							</div>
						</div>
					</div>

					<div class="custom-animated-circles">
						<div class="circle"></div>
						<div class="circle"></div>
						<div class="circle"></div>
						<div class="circle"></div>
					</div>

				</section>

				<div class="custom-screens-carousel">
					<div class="container">
						<div class="row justify-content-center text-center">
							<div class="col appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
								<div class="carousel-ipad">
									<div class="carousel-ipad-camera"></div>
									<div class="owl-carousel owl-theme nav-style-1 m-0" data-plugin-options="{'autoHeight': true, 'items': 1, 'margin': 10, 'nav': false, 'dots': false, 'stagePadding': 0, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 3000}">
										<div>
											<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-1.webp">
										</div>
										<div>
											<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-2.webp">
										</div>
										<div>
											<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-3.webp">
										</div>
									</div>
									<div class="carousel-ipad-home"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container my-5">
					<div class="row justify-content-center mb-4">
						<div class="col-md-12 col-lg-10">

							<div class="tabs tabs-bottom tabs-center tabs-simple custom-tabs-style-1 mt-2 mb-3">
								<ul class="nav nav-tabs mb-3">
									<li class="nav-item active">
										<a class="nav-link active" href="#tabsNavigationSimpleIcons1" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-0 m-0">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0" style="height: 100px;">
													<span class="box-content p-0 m-0">
														<!--<i class="icon-featured icon-bulb icons"></i>-->
														<img class="icon-featured icons" src="<?php echo base_url();?>public/images/website-under-construction.svg" alt="Desarrollo web">
													</span>
												</span>
											</span>									
											<p class="text-color-dark font-weight-bold mb-0 pt-2 text-2 pb-0">Desarrollo web</p>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigationSimpleIcons2" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-0 m-0">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0" style="height: 100px;">
													<span class="box-content p-0 m-0">
														<!--<i class="icon-featured icon-mustache icons"></i>-->
														<img class="icon-featured icons" src="<?php echo base_url();?>public/images/support-headset.svg" alt="Soporte">
													</span>
												</span>
											</span>									
											<p class="text-color-dark font-weight-bold mb-0 pt-2 text-2 pb-0">Soporte Tecnico</p>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigationSimpleIcons3" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-0 m-0">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0" style="height: 100px;">
													<span class="box-content p-0 m-0">
														<!--<i class="icon-featured icon-puzzle icons"></i>-->
														<img class="icon-featured icons" src="<?php echo base_url();?>public/images/browser-window-with-stats.svg" alt="pos">
													</span>
												</span>
											</span>									
											<p class="text-color-dark font-weight-bold mb-0 pt-2 text-2 pb-0">Puntos de venta</p>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigationSimpleIcons4" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-0 m-0">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0" style="height: 100px;">
													<span class="box-content p-0 m-0">
														<!--<i class="icon-featured icon-rocket icons"></i>-->
														<img class="icon-featured icons" src="<?php echo base_url();?>public/images/browser-window-with-stats.svg" alt="sistema">
													</span>
												</span>
											</span>									
											<p class="text-color-dark font-weight-bold mb-0 pt-2 text-2 pb-0">Estacionamiento</p>
										</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tabsNavigationSimpleIcons1">
										<div class="text-center">
											<p>Desarrollo de Software dedicado al control y manejo a la medida de la empresa.</p>
										</div>
									</div>
									<div class="tab-pane" id="tabsNavigationSimpleIcons2">
										<div class="text-center">
											<p>Mantenimiento correctivo y preventivo del equipo de computo.</p>
										</div>
									</div>
									<div class="tab-pane" id="tabsNavigationSimpleIcons3">
										<div class="text-center">
											<p>Software dedicado al control y manejo de la empresa con caracterisiticas especiales</p>
										</div>
										<div class="col text-center">
											<p>Para mayor informacion de <a class="btn btn-outline btn-primary text-1 font-weight-semibold text-uppercase px-5 btn-py-2 mb-3" href="<?php echo base_url();?>Puntodeventa">Punto de venta</a></p>
										</div>
									</div>
									<div class="tab-pane" id="tabsNavigationSimpleIcons4">
										<div class="text-center">
											<p>Sistema de estacionamiento para el control ingreso y egreso de vehículos</p>
										</div>
										<div class="col text-center">
											<p>Para mayor informacion de <a class="btn btn-outline btn-primary text-1 font-weight-semibold text-uppercase px-5 btn-py-2 mb-3" href="<?php echo base_url();?>sistema_de_estacionamientos">Estacionamiento</a></p>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
				<div class="container my-5">
					<div class="row">
						<div class="col">
							<div class="divider">
								<div class="divider-small-text">
									
								</div>
							</div>
						</div>
					</div>
					<div class="row text-center mt-5 pb-3">
						<div class="owl-carousel owl-theme carousel-center-active-item" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
							<div>
								<img style="height: 84px !important" class="img-fluid" src="<?php echo base_url();?>public/img/logos/jquery.png" alt="">
							</div>
							<div>
								<img style="height: 84px !important" class="img-fluid" src="<?php echo base_url();?>public/img/logos/javascript.png" alt="">
							</div>
							<div>
								<img style="height: 84px !important" class="img-fluid" src="<?php echo base_url();?>public/img/logos/css3.png" alt="">
							</div>
							<div>
								<img style="height: 84px !important" class="img-fluid" src="<?php echo base_url();?>public/img/logos/php.png" alt="">
							</div>
							<div>
								<img style="height: 84px !important" class="img-fluid" src="<?php echo base_url();?>public/img/logos/git.png" alt="">
							</div>
							<div>
								<img style="height: 84px !important" class="img-fluid" src="<?php echo base_url();?>public/img/logos/html5.png" alt="">
							</div>
							<div>
								<img style="height: 84px !important" class="img-fluid" src="<?php echo base_url();?>public/img/logos/mysql.png"  alt="">
							</div>
						</div>
					</div>
				</div>
				<!--
				<section class="section m-0 mt-4 border-0 p-relative" id="features">
					<div class="container">
						<div class="row py-2 align-items-center justify-content-center">
							<div class="col-lg-6 text-center">
								<img class="img-fluid" src="<?php echo base_url();?>public/img/demos/sass/features/feature-1.gif" alt="">
							</div>
							<div class="col-lg-6 text-center text-lg-left">
								<h2 class="text-color-dark font-weight-bold text-6 mb-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="0">Manage any Project is Easy</h2>
								<p class="lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim. Nullam id varius nunc. </p>
								<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc. Vivamus bibendum magna ex, et faucibus lacus venenatis.</p>
								<a href="<?php echo base_url();?>public/#" class="btn btn-dark custom-secondary-font font-weight-bold text-3 px-5 py-3 mt-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">VIEW OUR PLANS</a>
							</div>
						</div>
					</div>

					<div class="custom-animated-circles custom-animated-circles-primary custom-animated-circles-pos-3 d-none d-block-md">
						<div class="circle"></div>
						<div class="circle"></div>
						<div class="circle"></div>
						<div class="circle"></div>
					</div>
				</section>
				<section class="section bg-transparent m-0 border-0">
					<div class="container">
						<div class="row py-2 align-items-center justify-content-center">
							<div class="col-lg-6 text-center text-lg-left">
								<h2 class="text-color-dark font-weight-bold text-6 mb-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="0">Drag n’ Drop Features</h2>
								<p class="lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim. Nullam id varius nunc.</p>
								<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc. Vivamus bibendum magna ex, et faucibus lacus venenatis eget.</p>
								<a href="<?php echo base_url();?>public/#" class="btn btn-dark custom-secondary-font font-weight-bold text-3 px-5 py-3 mt-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">VIEW OUR PLANS</a>
							</div>
							<div class="col-lg-6 text-center">
								<img class="img-fluid" src="<?php echo base_url();?>public/img/demos/sass/features/feature-2.gif" alt="">
							</div>
						</div>
					</div>
				</section>
				<!--
				<section class="section m-0 border-0">
					<div class="container">
						<div class="row py-2 align-items-center justify-content-center">
							<div class="col-lg-6 text-center">
								<img class="img-fluid" src="<?php echo base_url();?>public/img/demos/sass/features/feature-3.gif" alt="">
							</div>
							<div class="col-lg-6 text-center text-lg-left">
								<h2 class="text-color-dark font-weight-bold text-6 mb-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="0">Advanced Reporting Features</h2>
								<p class="lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim.</p>
								<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc.</p>
								<a href="<?php echo base_url();?>public/#" class="btn btn-dark custom-secondary-font font-weight-bold text-3 px-5 py-3 mt-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">VIEW OUR PLANS</a>
							</div>
						</div>
					</div>
				</section>-->
				<!--
				<section class="section bg-dark section-height-3 m-0 border-0" id="overview">
					<div class="container-fluid">
						<div class="row">
							<div class="col text-center">
								<h2 class="text-color-light font-weight-bold text-7 mb-1">Control your Business like a Pro</h2>
								<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
					</div>

					<div class="row justify-content-center">
						<div class="col-11 py-4">
							
							<div class="owl-carousel owl-theme stage-margin stage-margin-lg nav-style-2 mb-3 mt-4" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 2}, '1199': {'items': 2}}, 'margin': 50, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 100, 'autoplay': true, 'autoplayTimeout': 5000}">
								<div>
									<div class="carousel-ipad carousel-ipad-sm">
										<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-1.jpg">
									</div>
								</div>
								<div>
									<div class="carousel-ipad carousel-ipad-sm">
										<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-2.jpg">
									</div>
								</div>
								<div>
									<div class="carousel-ipad carousel-ipad-sm">
										<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-3.jpg">
									</div>
								</div>
								<div>
									<div class="carousel-ipad carousel-ipad-sm">
										<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-4.jpg">
									</div>
								</div>
								<div>
									<div class="carousel-ipad carousel-ipad-sm">
										<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-5.jpg">
									</div>
								</div>
								<div>
									<div class="carousel-ipad carousel-ipad-sm">
										<img alt="" class="img-fluid rounded-0" src="<?php echo base_url();?>public/img/demos/sass/screens/screen-6.jpg">
									</div>
								</div>
							</div>

						</div>
					</div>

				</section>-->
				<!--
				<section class="section section-height-3 m-0 border-0" id="reviews">
					<div class="container">
						<div class="row py-2">
							<div class="col text-center">
								<h2 class="text-color-dark font-weight-bold text-7 mb-1">User Reviews</h2>
								<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim.</p>
							</div>
						</div>
						<div class="row pt-3">
							<div class="col-md-4 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="500">
								<div class="testimonial testimonial-light">
									<blockquote class="blockquote-default">
										<p class="mb-0 text-default">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula.</p>
									</blockquote>
									<div class="testimonial-arrow-down"></div>
									<div class="testimonial-author">
										<div class="testimonial-author-thumbnail">
											<img src="<?php echo base_url();?>public/img/clients/client-1.jpg" class="rounded-circle" alt="">
										</div>
										<p><strong class="font-weight-extra-bold text-dark">John Smith</strong><span class="text-default">CEO & Founder - Okler</span></p>
									</div>
								</div>
							</div>
							<div class="col-md-4 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="300">
								<div class="testimonial testimonial-primary">
									<blockquote>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
									</blockquote>
									<div class="testimonial-arrow-down"></div>
									<div class="testimonial-author">
										<div class="testimonial-author-thumbnail">
											<img src="<?php echo base_url();?>public/img/clients/client-2.jpg" class="rounded-circle" alt="">
										</div>
										<p><strong class="font-weight-extra-bold">Jessica Smith</strong><span>CEO & Founder - Okler</span></p>
									</div>
								</div>
							</div>
							<div class="col-md-4 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="500">
								<div class="testimonial testimonial-light">
									<blockquote class="blockquote-default">
										<p class="mb-0 text-default">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula.</p>
									</blockquote>
									<div class="testimonial-arrow-down"></div>
									<div class="testimonial-author">
										<div class="testimonial-author-thumbnail">
											<img src="<?php echo base_url();?>public/img/clients/client-3.jpg" class="rounded-circle" alt="">
										</div>
										<p><strong class="font-weight-extra-bold text-dark">Paul Smith</strong><span class="text-default">CEO & Founder - Okler</span></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>-->
				
				<!--
				<section class="section section-height-3 bg-transparent m-0 border-0" id="pricing">
					<div class="container">
						<div class="row py-2">
							<div class="col text-center">
								<h2 class="text-color-dark font-weight-bold text-7 mb-1">Pricing</h2>
								<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim.</p>
							</div>
						</div>

						<div class="pricing-table py-5">
							<div class="col-md-6 col-lg-3">
								<div class="plan">
									<div class="plan-header">
										<h3>Enterprise</h3>
									</div>
									<div class="plan-price">
										<span class="price"><span class="price-unit">$</span>59</span>
										<label class="price-label">PER MONTH</label>
									</div>
									<div class="plan-features">
										<ul>
											<li>10GB Disk Space</li>
											<li>100GB Monthly Bandwith</li>
											<li>20 Email Accounts</li>
											<li>Unlimited Subdomains</li>
										</ul>
									</div>
									<div class="plan-footer">
										<a href="<?php echo base_url();?>public/#" class="btn btn-dark btn-modern btn-outline py-2 px-4">Sign Up</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-3">
								<div class="plan">
									<div class="plan-header">
										<h3>Professional</h3>
									</div>
									<div class="plan-price">
										<span class="price"><span class="price-unit">$</span>29</span>
										<label class="price-label">PER MONTH</label>
									</div>
									<div class="plan-features">
										<ul>
											<li>5GB Disk Space</li>
											<li>50GB Monthly Bandwith</li>
											<li>10 Email Accounts</li>
											<li>Unlimited Subdomains</li>
										</ul>
									</div>
									<div class="plan-footer">
										<a href="<?php echo base_url();?>public/#" class="btn btn-dark btn-modern btn-outline py-2 px-4">Sign Up</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-3">
								<div class="plan plan-featured">
									<div class="plan-header bg-primary">
										<h3>Standard</h3>
									</div>
									<div class="plan-price">
										<span class="price"><span class="price-unit">$</span>17</span>
										<label class="price-label">PER MONTH</label>
									</div>
									<div class="plan-features">
										<ul>
											<li>3GB Disk Space</li>
											<li>25GB Monthly Bandwith</li>
											<li>5 Email Accounts</li>
											<li>Unlimited Subdomains</li>
										</ul>
									</div>
									<div class="plan-footer">
										<a href="<?php echo base_url();?>public/#" class="btn btn-primary btn-modern py-2 px-4">Sign Up</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-3">
								<div class="plan">
									<div class="plan-header">
										<h3>Basic</h3>
									</div>
									<div class="plan-price">
										<span class="price"><span class="price-unit">$</span>9</span>
										<label class="price-label">PER MONTH</label>
									</div>
									<div class="plan-features">
										<ul>
											<li>1GB Disk Space</li>
											<li>10GB Monthly Bandwith</li>
											<li>2 Email Accounts</li>
											<li>Unlimited Subdomains</li>
										</ul>
									</div>
									<div class="plan-footer">
										<a href="<?php echo base_url();?>public/#" class="btn btn-dark btn-modern btn-outline py-2 px-4">Sign Up</a>
									</div>
								</div>
							</div>
						</div>

					</div>
				</section>-->
				
				<!--
				<section class="section section-height-3 curved-border m-0 border-0" id="faqs">
					<div class="container">
						<div class="row py-2">
							<div class="col text-center">
								<h2 class="text-color-dark font-weight-bold text-7 mb-1">Frequent Asked Questions</h2>
								<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim.</p>
							</div>
						</div>
						<div class="row py-3 align-items-center justify-content-center">
							<div class="col-sm-9">
								
								<div class="toggle toggle-minimal toggle-primary" data-plugin-toggle>
									<section class="toggle active">
										<label>Curabitur eget leo at velit imperdiet vague iaculis vitaes?</label>
										<div class="toggle-content">
											<p class="pb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="<?php echo base_url();?>public/#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="<?php echo base_url();?>public/#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet.</p>
										</div>
									</section>
									<section class="toggle">
										<label>Curabitur eget leo at imperdiet vague iaculis vitaes?</label>
										<div class="toggle-content">
											<p class="pb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor.</p>
										</div>
									</section>
									<section class="toggle">
										<label>Eget leo at imperdiet vague iaculis vitaes?</label>
										<div class="toggle-content">
											<p class="pb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor.</p>
										</div>
									</section>
									<section class="toggle">
										<label>Leo at imperdiet vague iaculis vitaes?</label>
										<div class="toggle-content">
											<p class="pb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor.</p>
										</div>
									</section>
									<section class="toggle">
										<label>Eget leo at imperdiet vague iaculis vitaes?</label>
										<div class="toggle-content">
											<p class="pb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor.</p>
										</div>
									</section>
									<section class="toggle">
										<label>Imperdiet vague iaculis vitaes?</label>
										<div class="toggle-content">
											<p class="pb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor.</p>
										</div>
									</section>
									<section class="toggle">
										<label>Curabitur eget leo at velit imperdiet vague iaculis vitaes?</label>
										<div class="toggle-content">
											<p class="pb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="<?php echo base_url();?>public/#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet.</p>
										</div>
									</section>
								</div>

							</div>
						</div>
					</div>
				</section>-->
				

				<section class="section section-height-3 bg-transparent m-0 border-0">
					<div class="container">
						<div class="row justify-content-center counters counters-lg pt-2">
							<div class="col-md-3 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="500">
								<div class="counter counter-with-unit counter-unit-on-top">
									<strong class="text-color-dark font-weight-extra-bold text-13 text-lg-15" data-to="500">0</strong>
									<!--<strong class="unit text-color-dark font-weight-bold text-5 text-lg-8">%</strong>-->
									<label class="font-weight-normal text-3 text-lg-4 px-5 px-lg-4">CLIENTES</label>
								</div>
							</div>
							<div class="col-md-4 col-lg-3 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="300">
								<div class="counter counter-with-unit counter-unit-on-top">
									<strong class="text-color-dark font-weight-extra-bold text-13 text-lg-15" data-to="240000">0</strong>
									<!--<strong class="unit text-color-dark font-weight-bold text-5 text-lg-8">+</strong>-->
									<label class="font-weight-normal text-3 text-lg-4 px-5">HORAS PROGRAMANDO</label>
								</div>
							</div>
							<div class="col-md-3 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="500">
								<div class="counter counter-with-unit counter-unit-on-top">
									<strong class="text-color-dark font-weight-extra-bold text-13 text-lg-15" data-to="8">0</strong>
									<!--<strong class="unit text-color-dark font-weight-bold text-5 text-lg-8">+</strong>-->
									<label class="font-weight-normal text-3 text-lg-4 px-5">AÑOS DESARROLLANDO</label>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="section section-height-2 bg-primary curved-border curved-border-top overflow-hidden border-0 m-0 backblueinter" id="trial" style="padding-bottom: 0px;">
					<div class="custom-animated-circles custom-animated-circles-pos-2">
						<div class="circle"></div>
						<div class="circle"></div>
						<div class="circle"></div>
						<div class="circle"></div>
					</div>

					<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-2.png" class="img-fluid position-absolute" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 3, 'transition': true}" style="top: 29%; left: 26%;" />
					<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-5.png" class="img-fluid position-absolute" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 3.5, 'transition': true, 'horizontal': true}" style="top: 36%; left: 80%;" />
					<img src="<?php echo base_url();?>public/img/demos/sass/icons/icon-6.png" class="img-fluid position-absolute" alt="" data-plugin-float-element data-plugin-options="{'startPos': 'none', 'speed': 2.5, 'transition': true}" style="top: 57%; left: 73%;" />

					<div class="container pb-5 mb-0">
						<div class="row pb-2 mb-4">
							<div class="col text-center">
								<h2 class="text-color-light font-weight-bold text-10 mb-2">Contacto</h2>
								
							</div>
						</div>
						<div class="row justify-content-center ">
							<div class="col-lg-8">
								<form id="fomcontacto" class="contact-form custom-form-style-1 form-errors-light" action="php/contact-form.php" method="POST">
									<div class="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
										<strong>Success!</strong> Mensaje enviado.
									</div>

									<div class="contact-form-error alert alert-danger d-none mt-4" id="contactError">
										<strong>Error!</strong> There was an error sending your message.
										<span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
									</div>
									<div class="contact-form-error alert alert-danger d-none mt-4" id="contactErrorrecapch">
										<strong>Error!</strong> Porfavor verifique Recaptch.
										<span class="mail-error-message text-1 d-block" id="mailErrorMessage2"></span>
									</div>
									
									<div class="form-row">
										<div class="form-group col-md-6 mb-2">
											<input type="text" value="" data-msg-required="Por favor agregue un nombre." maxlength="100" class="form-control" name="nombre" id="nombre" placeholder="Nombre" required>
										</div>
										<div class="form-group col-md-6 mb-2">
											<input type="mail" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="correo" id="correo" placeholder="Correo" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6 mb-2">
											<input type="tel" value="" data-msg-required="Por favor agrege un telefono" maxlength="10" class="form-control" name="telefono" id="telefono" placeholder="Telefono" required>
										</div>
										<div class="form-group col-md-6 mb-2">
											<input type="text" value="" data-msg-required="Por favor agregre un asunto." maxlength="150" class="form-control" name="Asunto" id="asunto" placeholder="asunto" required>
										</div>
										
									</div>

									<div class="form-row">
										<div class="form-group col mb-2">
											<textarea data-msg-required="Por favor agregue un mensaje." class="form-control" name="mensaje" id="mensaje" placeholder="Mensaje" required style="min-height: 70px"></textarea>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col text-center">
											<div class="g-recaptcha" data-sitekey="6Ld5YakUAAAAAAlTGkYdqZm_ybiKr40Kbk5v2rvL"></div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col text-center">
											<button class="btn btn-dark btn-modern custom-secondary-font text-3 px-5 py-3 contactosubmit" data-loading-text="Loading...">Enviar</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
				<!--
				<div class="container">
					<div class="row" style="margin-top: -130px;">
						
						<div class="col-md-4">
							<a href="<?php echo base_url();?>public/#" class="text-decoration-none">
								<div class="card border-0 border-radius-0 custom-box-shadow-1">
									<div class="card-body text-center p-5 my-2">
										<i class="icon-screen-smartphone icons text-color-dark text-11 mb-3 d-block"></i>
										<h4 class="text-color-primary font-weight-bold text-4 pb-1 mb-2">APP AVAILABLE</h4>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit phasellus.</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?php echo base_url();?>public/#" class="text-decoration-none">
								<div class="card border-0 border-radius-0 custom-box-shadow-1">
									<div class="card-body text-center p-5 my-2">
										<i class="icon-magnifier icons text-color-dark text-11 mb-3 d-block"></i>
										<h4 class="text-color-primary font-weight-bold text-4 pb-1 mb-2">KNOWLEDGE BASE</h4>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit phasellus.</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?php echo base_url();?>public/#" class="text-decoration-none">
								<div class="card border-0 border-radius-0 custom-box-shadow-1">
									<div class="card-body text-center p-5 my-2">
										<i class="icon-screen-desktop icons text-color-dark text-11 mb-3 d-block"></i>
										<h4 class="text-color-primary font-weight-bold text-4 pb-1 mb-2">USERS FORUM</h4>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit phasellus.</p>
									</div>
								</div>
							</a>
						</div>

					</div>
				</div>-->

		<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>	