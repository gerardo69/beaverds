$(document).ready(function($) {
	
});
var base_url=$('#base_url').val();
$(document).ready(function($) {
	VANTA.WAVES({
	  el: "#intro",
	  shininess: 53.00,
	  waveHeight: 11.00,
	  waveSpeed: 0.70,
	  zoom: 0.65
	});
	VANTA.WAVES({
	  el: ".backblueinter",
	  mouseControls: true,
  touchControls: true,
  minHeight: 200.00,
  minWidth: 200.00,
  scale: 1.00,
  scaleMobile: 1.00
	});
	//------------------------------------------------------------------------
	var form_register = $('#fomcontacto');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                captcha: {
					required: true,
					//remote: "process.php"
				}
                
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    //success_register.fadeOut(500);
                    //error_register.fadeIn(500);
                    //scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
	$('.contactosubmit').click(function(event) {
		var varlid=$('#fomcontacto').valid();
		if (varlid) {
			if (grecaptcha.getResponse() == "") {
	            flag = 1;
	            alert('Porfavor verifique Recaptch');
	        } else {
	            var datos = form_register.serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Contacto/enviada',
                    data: datos,
                    async: false,
                    statusCode:{
                        404: function(data){
                        	alert('Error de ruta');
                        },
                        500: function(){
                            alert('Error 500');
                        }
                    },
                    success:function(data){
                    	$('#contactSuccess').show('show');
                        alert('Mensaje enviado');
                        setInterval(function(){ 
                        location.href='';
                        }, 3000);

                    }
                });
	        }
			
		}
	});	
});