(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Símbolo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRAFIAAgJIAjAAIAAAJg");
	this.shape.setTransform(94.6,189.4,3.317,2.62);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_1.setTransform(121.3,189.4,3.317,2.62);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgWAFIAAgJIAtAAIAAAJg");
	this.shape_2.setTransform(135.9,182.6,3.317,2.62);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgSAFIAAgJIAlAAIAAAJg");
	this.shape_3.setTransform(116.7,182.6,3.317,2.62);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgkAFIAAgJIBJAAIAAAJg");
	this.shape_4.setTransform(70.7,189.4,3.317,2.62);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ah6AFIAAgJID1AAIAAAJg");
	this.shape_5.setTransform(62.9,182.6,3.317,2.62);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_6.setTransform(35.9,189.4,3.317,2.62);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgdAFIAAgJIA7AAIAAAJg");
	this.shape_7.setTransform(85.9,168.9,3.317,2.62);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgPAFIAAgJIAfAAIAAAJg");
	this.shape_8.setTransform(137.4,162.1,3.317,2.62);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag9AFIAAgJIB7AAIAAAJg");
	this.shape_9.setTransform(106.5,162.1,3.317,2.62);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgUAFIAAgJIApAAIAAAJg");
	this.shape_10.setTransform(155.1,155.4,3.317,2.62);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgVAFIAAgJIAqAAIAAAJg");
	this.shape_11.setTransform(134.2,155.4,3.317,2.62);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhdAFIAAgJIC8AAIAAAJg");
	this.shape_12.setTransform(89.6,155.4,3.317,2.62);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgfAFIAAgJIA/AAIAAAJg");
	this.shape_13.setTransform(69,162.1,3.317,2.62);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgqAFIAAgJIBVAAIAAAJg");
	this.shape_14.setTransform(69.5,148.6,3.317,2.62);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AiDAFIAAgJIEHAAIAAAJg");
	this.shape_15.setTransform(118.2,141.8,3.317,2.62);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_16.setTransform(53.2,141.8,3.317,2.62);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgoAFIAAgJIBQAAIAAAJg");
	this.shape_17.setTransform(54.7,168.9,3.317,2.62);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_18.setTransform(35.9,162.1,3.317,2.62);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_19.setTransform(35.9,155.4,3.317,2.62);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AhGAFIAAgJICOAAIAAAJg");
	this.shape_20.setTransform(23.7,148.6,3.317,2.62);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgrAFIAAgJIBXAAIAAAJg");
	this.shape_21.setTransform(14.7,141.8,3.317,2.62);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgRAFIAAgJIAjAAIAAAJg");
	this.shape_22.setTransform(94.6,130.6,3.317,2.62);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_23.setTransform(121.3,130.6,3.317,2.62);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgWAFIAAgKIAtAAIAAAKg");
	this.shape_24.setTransform(135.9,123.8,3.317,2.62);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgSAFIAAgKIAlAAIAAAKg");
	this.shape_25.setTransform(116.7,123.8,3.317,2.62);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgkAFIAAgJIBJAAIAAAJg");
	this.shape_26.setTransform(70.7,130.6,3.317,2.62);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AhEAFIAAgKICJAAIAAAKg");
	this.shape_27.setTransform(81,123.8,3.317,2.62);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_28.setTransform(35.9,130.6,3.317,2.62);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgpAFIAAgKIBTAAIAAAKg");
	this.shape_29.setTransform(35.9,123.8,3.317,2.62);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgQAFIAAgJIAhAAIAAAJg");
	this.shape_30.setTransform(148.8,110.2,3.317,2.62);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AiQAFIAAgJIEhAAIAAAJg");
	this.shape_31.setTransform(89.5,110.2,3.317,2.62);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgdAFIAAgJIA7AAIAAAJg");
	this.shape_32.setTransform(85.9,110.2,3.317,2.62);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgPAFIAAgJIAfAAIAAAJg");
	this.shape_33.setTransform(137.4,103.4,3.317,2.62);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("Ag9AFIAAgJIB7AAIAAAJg");
	this.shape_34.setTransform(106.5,103.4,3.317,2.62);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AhdAFIAAgJIC8AAIAAAJg");
	this.shape_35.setTransform(89.6,96.6,3.317,2.62);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgfAFIAAgJIA/AAIAAAJg");
	this.shape_36.setTransform(69,103.4,3.317,2.62);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgqAFIAAgJIBVAAIAAAJg");
	this.shape_37.setTransform(69.5,89.8,3.317,2.62);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AhCAFIAAgJICFAAIAAAJg");
	this.shape_38.setTransform(119.8,83,3.317,2.62);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AhJAFIAAgJICTAAIAAAJg");
	this.shape_39.setTransform(63.9,83,3.317,2.62);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_40.setTransform(35.9,103.4,3.317,2.62);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_41.setTransform(35.9,96.6,3.317,2.62);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AhGAFIAAgJICOAAIAAAJg");
	this.shape_42.setTransform(23.7,89.8,3.317,2.62);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgrAFIAAgJIBXAAIAAAJg");
	this.shape_43.setTransform(14.7,83,3.317,2.62);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgRAFIAAgJIAjAAIAAAJg");
	this.shape_44.setTransform(94.6,55.7,3.317,2.62);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgqAFIAAgJIBVAAIAAAJg");
	this.shape_45.setTransform(129.4,69.4,3.317,2.62);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgdAFIAAgJIA7AAIAAAJg");
	this.shape_46.setTransform(152.1,62.5,3.317,2.62);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AhPAFIAAgJICfAAIAAAJg");
	this.shape_47.setTransform(108.6,62.5,3.317,2.62);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AhLAFIAAgJICYAAIAAAJg");
	this.shape_48.setTransform(83.6,69.4,3.317,2.62);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgXAFIAAgJIAvAAIAAAJg");
	this.shape_49.setTransform(66.3,62.5,3.317,2.62);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_50.setTransform(35.9,69.4,3.317,2.62);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_51.setTransform(35.9,62.5,3.317,2.62);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_52.setTransform(121.3,55.7,3.317,2.62);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgWAFIAAgJIAtAAIAAAJg");
	this.shape_53.setTransform(135.9,48.9,3.317,2.62);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgSAFIAAgJIAlAAIAAAJg");
	this.shape_54.setTransform(116.7,48.9,3.317,2.62);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AhHAFIAAgJICPAAIAAAJg");
	this.shape_55.setTransform(82,48.9,3.317,2.62);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgkAFIAAgJIBJAAIAAAJg");
	this.shape_56.setTransform(70.7,55.7,3.317,2.62);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_57.setTransform(35.9,55.7,3.317,2.62);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_58.setTransform(35.9,48.9,3.317,2.62);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgdAFIAAgJIA7AAIAAAJg");
	this.shape_59.setTransform(85.9,35.3,3.317,2.62);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgPAFIAAgJIAfAAIAAAJg");
	this.shape_60.setTransform(137.4,28.5,3.317,2.62);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("Ag0AFIAAgJIBpAAIAAAJg");
	this.shape_61.setTransform(109.5,28.5,3.317,2.62);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgdAFIAAgJIA7AAIAAAJg");
	this.shape_62.setTransform(152.1,21.7,3.317,2.62);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgYAFIAAgJIAxAAIAAAJg");
	this.shape_63.setTransform(127,21.7,3.317,2.62);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgtAFIAAgJIBbAAIAAAJg");
	this.shape_64.setTransform(97.4,21.7,3.317,2.62);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_65.setTransform(72.1,28.5,3.317,2.62);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AhOAFIAAgJICdAAIAAAJg");
	this.shape_66.setTransform(48.2,21.7,3.317,2.62);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgqAFIAAgJIBVAAIAAAJg");
	this.shape_67.setTransform(69.5,14.9,3.317,2.62);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgwAFIAAgJIBhAAIAAAJg");
	this.shape_68.setTransform(145.9,8.2,3.317,2.62);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AgjAFIAAgJIBHAAIAAAJg");
	this.shape_69.setTransform(108.6,8.2,3.317,2.62);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AhLAFIAAgJICXAAIAAAJg");
	this.shape_70.setTransform(64.4,8.2,3.317,2.62);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AgoAFIAAgJIBQAAIAAAJg");
	this.shape_71.setTransform(54.7,35.3,3.317,2.62);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgpAFIAAgJIBTAAIAAAJg");
	this.shape_72.setTransform(35.9,28.5,3.317,2.62);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AhGAFIAAgJICOAAIAAAJg");
	this.shape_73.setTransform(23.7,14.9,3.317,2.62);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AgrAFIAAgJIBXAAIAAAJg");
	this.shape_74.setTransform(14.7,8.2,3.317,2.62);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AhTAFIAAgJICnAAIAAAJg");
	this.shape_75.setTransform(27.9,1.4,3.317,2.62);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Símbolo2, new cjs.Rectangle(0,0,162.1,190.8), null);


// stage content:
(lib.laptop = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mascara (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AvjIEIAAwHIfHAAIAAQHg");
	var mask_graphics_79 = new cjs.Graphics().p("AvjIEIAAwHIfHAAIAAQHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:192.4,y:144.2}).wait(79).to({graphics:mask_graphics_79,x:192.4,y:144.2}).wait(1));

	// OBJECTS copia
	this.instance = new lib.Símbolo2();
	this.instance.parent = this;
	this.instance.setTransform(188.2,187.6,1,1,0,0,0,81,95.4);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:186.6},0).wait(1).to({y:185.6},0).wait(1).to({y:184.6},0).wait(1).to({y:183.6},0).wait(1).to({y:182.6},0).wait(1).to({y:181.6},0).wait(1).to({y:180.6},0).wait(1).to({y:179.6},0).wait(1).to({y:178.6},0).wait(1).to({y:177.6},0).wait(1).to({y:176.6},0).wait(1).to({y:175.6},0).wait(1).to({y:174.6},0).wait(1).to({y:173.6},0).wait(1).to({y:172.6},0).wait(1).to({y:171.6},0).wait(1).to({y:170.6},0).wait(1).to({y:169.6},0).wait(1).to({y:168.6},0).wait(1).to({y:167.6},0).wait(1).to({y:166.6},0).wait(1).to({y:165.6},0).wait(1).to({y:164.6},0).wait(1).to({y:163.6},0).wait(1).to({y:162.6},0).wait(1).to({y:161.6},0).wait(1).to({y:160.6},0).wait(1).to({y:159.6},0).wait(1).to({y:158.6},0).wait(1).to({y:157.6},0).wait(1).to({y:156.6},0).wait(1).to({y:155.6},0).wait(1).to({y:154.6},0).wait(1).to({y:153.6},0).wait(1).to({y:152.6},0).wait(1).to({y:151.6},0).wait(1).to({y:150.6},0).wait(1).to({y:149.6},0).wait(1).to({y:148.6},0).wait(1).to({y:147.6},0).wait(1).to({y:146.6},0).wait(1).to({y:145.6},0).wait(1).to({y:144.6},0).wait(1).to({y:143.6},0).wait(1).to({y:142.6},0).wait(1).to({y:141.6},0).wait(1).to({y:140.6},0).wait(1).to({y:139.6},0).wait(1).to({y:138.6},0).wait(1).to({y:137.6},0).wait(1).to({y:136.6},0).wait(1).to({y:135.6},0).wait(1).to({y:134.6},0).wait(1).to({y:133.6},0).wait(1).to({y:132.6},0).wait(1).to({y:131.6},0).wait(1).to({y:130.6},0).wait(1).to({y:129.6},0).wait(1).to({y:128.6},0).wait(1).to({y:127.6},0).wait(1).to({y:126.6},0).wait(1).to({y:125.6},0).wait(1).to({y:124.6},0).wait(1).to({y:123.6},0).wait(1).to({y:122.6},0).wait(1).to({y:121.6},0).wait(1).to({y:120.6},0).wait(1).to({y:119.6},0).wait(1).to({y:118.6},0).wait(1).to({y:117.6},0).wait(1).to({y:116.6},0).wait(1).to({y:115.6},0).wait(1).to({y:114.6},0).wait(1).to({y:113.6},0).wait(1).to({y:112.6},0).wait(1).to({y:111.6},0).wait(1).to({y:110.6},0).wait(1).to({y:109.6},0).wait(1).to({y:108.6},0).wait(1));

	// OBJECTS
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#17ACB3").s().p("AlOAaIAAgzIKdAAIAAAzg");
	this.shape.setTransform(198.4,83.3,3.329,1.364);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3E3A39").s().p("AlOG4IAAtvIKdAAIAANvg");
	this.shape_1.setTransform(198.4,139.8,3.329,1.364);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_1},{t:this.shape}]},79).wait(1));

	// OBJECTS
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66646D").s().p("A3WAnQg1AAgrgWQgrgUgVgjMAztAAAQgXAmgxAVIgBABIgBAAIAAAAIAAAAIgBABIgBAAQgmAQgrAAg");
	this.shape_2.setTransform(194.5,233.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00A195").s().p("AzQA9IAAh5MAmhAAAIAAB5g");
	this.shape_3.setTransform(193.9,65.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E2E4E8").s().p("AzQKdIAA05MAmhAAAIAAU5g");
	this.shape_4.setTransform(193.9,138.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#D1CCE5").s().p("A3WBWQhJAAg0goQg0goAAg4IAAgjMA0PAAAIAAAjQAAA4gzAoQg0AohKAAg");
	this.shape_5.setTransform(194.5,228.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000404").s().p("AyxOsQg7AAgqgqQgqgqAAg7IAA45QAAg7AqgqQAqgqA7AAMAljAAAQA7AAAqAqQAqAqAAA7IAAY5QAAA7gqAqQgqAqg7AAg");
	this.shape_6.setTransform(194.5,141.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]},79).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.3,189.7,334.5,189.8);
// library properties:
lib.properties = {
	width: 388,
	height: 284,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;